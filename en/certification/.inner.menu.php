<?
$aMenuLinks = Array(
	Array(
		"Certificates of the Unified Energy System of Ukraine", 
		"/en/certification/certificates-eeu/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Certificates of the Republic of Belarus", 
		"/en/certification/certificates-rb/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Security Certificate", 
		"/en/certification/sb-certificate/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Certificate of the EU", 
		"/en/certification/eu-certificate/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"International Certificates", 
		"/en/certification/international-certificate/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Certification of services", 
		"/en/certification/certification-services/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Copies of certificates", 
		"/en/certification/copies-of-certificates/", 
		Array(), 
		Array(), 
		"" 
	)
);
?>