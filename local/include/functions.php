<?php

namespace bellis;

/**
 * Возвращает скрытые пункты меню
 * @param array $items
 */
function hideMenuItems (array $items) {
    
    return $items;
}

/**
 * Возвращает отформатированную дату в зависимости от языка сайта
 * @param string $date
 * @return string
 */
function formateDateByLang ($date) {
    
    if (LANGUAGE_ID == 'ru') {
        
        return FormatDate('j F Y', MakeTimeStamp($date));
    } elseif (LANGUAGE_ID == 'en') {
        
        return date("d F Y", MakeTimeStamp($date));
    }
}

/**
 * Возвращает название элемента по id в зависимости от языка сайта
 * @param type $id
 * @return string
 */
function nameByElementId ($id) {
    
    \Bitrix\Main\Loader::includeModule('iblock');
    $dbElement = \CIBlockElement::GetByID(intVal($id))->GetNextElement();
    
    if (!$dbElement) {
        
        return '';
    }
    
    if (defined("POSTFIX_PROPERTY") && strlen(POSTFIX_PROPERTY) > 0) {
        
        $properties = $dbElement->GetProperties();
        return $properties['NAME' . POSTFIX_PROPERTY]['VALUE'];
    }
    
    $arFields = $dbElement->GetFields();
    return $arFields['NAME'];
}

/**
 * Возвращает имя элемента в зависимости от языка сайта
 * @param array $arr
 * @return string
 */
function nameByLang (array $arr) {
    
    return fieldByLang($arr, 'NAME');
}

/**
 * Возвращает значение поля в зависимости от языка сайта
 * @param array $arr
 * @param type $field
 * @return mixed
 */
function fieldByLang (array $arr, $field) {
    
    if (defined("POSTFIX_PROPERTY") && strlen(POSTFIX_PROPERTY) > 0) {
        
        return _getFieldValue($arr, $field);
    }
    
    if (isset($arr[$field])) {
        
        return $arr[$field];
    }
    
    return _getFieldValue($arr, $field);
}

/**
 * Возвращает название раздела в зависимости от языка сайта
 * @param type $arr
 * @return string
 */
function sectionNameByLang ($arr) {
    
    if (defined("POSTFIX_PROPERTY") && strlen(POSTFIX_PROPERTY) > 0) {
        
        return $arr['UF_NAME' . POSTFIX_PROPERTY];
    }
    
    return $arr['NAME'];
}

/**
 * Возвращает значение поля
 * @param type $arr
 * @param type $field
 * @return type
 */
function _getFieldValue($arr, $field) {
    
    if ($arr['PROPERTIES'][$field . POSTFIX_PROPERTY]['USER_TYPE'] == 'HTML') {
        return $arr['PROPERTIES'][$field . POSTFIX_PROPERTY]['~VALUE']['TEXT'];
    }
    return $arr['PROPERTIES'][$field . POSTFIX_PROPERTY]['~VALUE'];
}

/**
 * Dump переменной
 * @param type $var
 * @param type $var_dump
 */
function dm ($var, $var_dump = false) {
    
    echo '<pre>';
    if ($var_dump) {
        var_dump($var);
    } else {
        print_r($var);
    }
    echo '</pre>';
}