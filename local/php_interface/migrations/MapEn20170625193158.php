<?php

namespace Sprint\Migration;

class MapEn20170625193158 extends Version {

    protected $description = "MapEn";

    public function up() {
        $helper = new HelperManager();

        $helper->Iblock()->addIblockTypeIfNotExists(array(
            'ID' => 'bellis',
            'SECTIONS' => 'Y',
            'EDIT_FILE_BEFORE' => '',
            'EDIT_FILE_AFTER' => '',
            'IN_RSS' => 'N',
            'SORT' => '500',
            'LANG' =>
            array(
                'ru' =>
                array(
                    'NAME' => 'Bellis',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ),
                'en' =>
                array(
                    'NAME' => 'Bellis',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ),
                'be' =>
                array(
                    'NAME' => 'Bellis',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ),
            ),
        ));

        $iblockId = $helper->Iblock()->addIblockIfNotExists(array(
            'IBLOCK_TYPE_ID' => 'bellis',
            'LID' => 's7',
            'CODE' => 'map_en',
            'NAME' => 'Map',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'LIST_PAGE_URL' => '#SITE_DIR#/contacts/#contacts-map',
            'DETAIL_PAGE_URL' => '#SITE_DIR#/contacts/#contacts-map',
            'SECTION_PAGE_URL' => '#SITE_DIR#/contacts/#contacts-map',
            'PICTURE' => NULL,
            'DESCRIPTION' => '',
            'DESCRIPTION_TYPE' => 'text',
            'RSS_TTL' => '24',
            'RSS_ACTIVE' => 'Y',
            'RSS_FILE_ACTIVE' => 'N',
            'RSS_FILE_LIMIT' => NULL,
            'RSS_FILE_DAYS' => NULL,
            'RSS_YANDEX_ACTIVE' => 'N',
            'XML_ID' => '',
            'TMP_ID' => NULL,
            'INDEX_ELEMENT' => 'Y',
            'INDEX_SECTION' => 'Y',
            'WORKFLOW' => 'N',
            'BIZPROC' => 'N',
            'SECTION_CHOOSER' => 'L',
            'LIST_MODE' => '',
            'RIGHTS_MODE' => 'S',
            'SECTION_PROPERTY' => 'N',
            'VERSION' => '1',
            'LAST_CONV_ELEMENT' => '0',
            'SOCNET_GROUP_ID' => NULL,
            'EDIT_FILE_BEFORE' => '',
            'EDIT_FILE_AFTER' => '',
            'SECTIONS_NAME' => 'Разделы',
            'SECTION_NAME' => 'Раздел',
            'ELEMENTS_NAME' => 'Элементы',
            'ELEMENT_NAME' => 'Элемент',
            'PROPERTY_INDEX' => 'N',
            'CANONICAL_PAGE_URL' => '',
            'EXTERNAL_ID' => '',
            'LANG_DIR' => '/',
            'SERVER_NAME' => 'bellis',
        ));

        $helper->Iblock()->updateIblockFields($iblockId, array(
            'IBLOCK_SECTION' =>
            array(
                'NAME' => 'Привязка к разделам',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' =>
                array(
                    'KEEP_IBLOCK_SECTION_ID' => 'N',
                ),
            ),
            'ACTIVE' =>
            array(
                'NAME' => 'Активность',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => 'Y',
            ),
            'ACTIVE_FROM' =>
            array(
                'NAME' => 'Начало активности',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'ACTIVE_TO' =>
            array(
                'NAME' => 'Окончание активности',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'SORT' =>
            array(
                'NAME' => 'Сортировка',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '0',
            ),
            'NAME' =>
            array(
                'NAME' => 'Название',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => '',
            ),
            'PREVIEW_PICTURE' =>
            array(
                'NAME' => 'Картинка для анонса',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' =>
                array(
                    'FROM_DETAIL' => 'N',
                    'SCALE' => 'N',
                    'WIDTH' => '',
                    'HEIGHT' => '',
                    'IGNORE_ERRORS' => 'N',
                    'METHOD' => 'resample',
                    'COMPRESSION' => 95,
                    'DELETE_WITH_DETAIL' => 'N',
                    'UPDATE_WITH_DETAIL' => 'N',
                    'USE_WATERMARK_TEXT' => 'N',
                    'WATERMARK_TEXT' => '',
                    'WATERMARK_TEXT_FONT' => '',
                    'WATERMARK_TEXT_COLOR' => '',
                    'WATERMARK_TEXT_SIZE' => '',
                    'WATERMARK_TEXT_POSITION' => 'tl',
                    'USE_WATERMARK_FILE' => 'N',
                    'WATERMARK_FILE' => '',
                    'WATERMARK_FILE_ALPHA' => '',
                    'WATERMARK_FILE_POSITION' => 'tl',
                    'WATERMARK_FILE_ORDER' => NULL,
                ),
            ),
            'PREVIEW_TEXT_TYPE' =>
            array(
                'NAME' => 'Тип описания для анонса',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => 'text',
            ),
            'PREVIEW_TEXT' =>
            array(
                'NAME' => 'Описание для анонса',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'DETAIL_PICTURE' =>
            array(
                'NAME' => 'Детальная картинка',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' =>
                array(
                    'SCALE' => 'N',
                    'WIDTH' => '',
                    'HEIGHT' => '',
                    'IGNORE_ERRORS' => 'N',
                    'METHOD' => 'resample',
                    'COMPRESSION' => 95,
                    'USE_WATERMARK_TEXT' => 'N',
                    'WATERMARK_TEXT' => '',
                    'WATERMARK_TEXT_FONT' => '',
                    'WATERMARK_TEXT_COLOR' => '',
                    'WATERMARK_TEXT_SIZE' => '',
                    'WATERMARK_TEXT_POSITION' => 'tl',
                    'USE_WATERMARK_FILE' => 'N',
                    'WATERMARK_FILE' => '',
                    'WATERMARK_FILE_ALPHA' => '',
                    'WATERMARK_FILE_POSITION' => 'tl',
                    'WATERMARK_FILE_ORDER' => NULL,
                ),
            ),
            'DETAIL_TEXT_TYPE' =>
            array(
                'NAME' => 'Тип детального описания',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => 'text',
            ),
            'DETAIL_TEXT' =>
            array(
                'NAME' => 'Детальное описание',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'XML_ID' =>
            array(
                'NAME' => 'Внешний код',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => '',
            ),
            'CODE' =>
            array(
                'NAME' => 'Символьный код',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' =>
                array(
                    'UNIQUE' => 'Y',
                    'TRANSLITERATION' => 'Y',
                    'TRANS_LEN' => 100,
                    'TRANS_CASE' => 'L',
                    'TRANS_SPACE' => '-',
                    'TRANS_OTHER' => '-',
                    'TRANS_EAT' => 'Y',
                    'USE_GOOGLE' => 'N',
                ),
            ),
            'TAGS' =>
            array(
                'NAME' => 'Теги',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
        ));

        $helper->Iblock()->addPropertyIfNotExists($iblockId, array(
            'NAME' => 'Точка на карте',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'MAP',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => '',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'TMP_ID' => NULL,
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '1',
            'USER_TYPE' => 'map_yandex',
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->addPropertyIfNotExists($iblockId, array(
            'NAME' => 'Адрес',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'ADDRESS',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => '',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'TMP_ID' => NULL,
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '1',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->addPropertyIfNotExists($iblockId, array(
            'NAME' => 'Точки пешеходного маршрута',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'WAY_POINTS_WALKING',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'Y',
            'XML_ID' => '',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'TMP_ID' => NULL,
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->addPropertyIfNotExists($iblockId, array(
            'NAME' => 'Точки маршрута на машине',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'WAY_POINTS_DRIVING',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'Y',
            'XML_ID' => '',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'TMP_ID' => NULL,
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => NULL,
            'USER_TYPE_SETTINGS' => NULL,
            'HINT' => '',
        ));
        $helper->Iblock()->addPropertyIfNotExists($iblockId, array(
            'NAME' => 'Текст на маркере точки A',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'MARKER_TEXT_A',
            'DEFAULT_VALUE' =>
            array(
                'TEXT' => '',
                'TYPE' => 'HTML',
            ),
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => '',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'TMP_ID' => NULL,
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '1',
            'USER_TYPE' => 'HTML',
            'USER_TYPE_SETTINGS' =>
            array(
                'height' => 200,
            ),
            'HINT' => '',
        ));
        $helper->Iblock()->addPropertyIfNotExists($iblockId, array(
            'NAME' => 'Текст на маркере точки B',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'MARKER_TEXT_B',
            'DEFAULT_VALUE' =>
            array(
                'TEXT' => '',
                'TYPE' => 'HTML',
            ),
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => '',
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'TMP_ID' => NULL,
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '1',
            'USER_TYPE' => 'HTML',
            'USER_TYPE_SETTINGS' =>
            array(
                'height' => 200,
            ),
            'HINT' => '',
        ));
        
        $helper->AdminIblock()->buildElementForm($iblockId, array(
            'Элемент' =>
            array(
                'ID' => 'ID',
                'ACTIVE' => 'Активность',
                'NAME' => 'Название',
                'PROPERTY_ADDRESS' => 'Адрес',
                'PROPERTY_ICON' => 'Иконка маркера',
                'PROPERTY_MARKER_TEXT' => 'Текст на маркере точки',
                'PROPERTY_MAP' => 'Точка на карте',
                'CODE' => 'Символьный код',
                'XML_ID' => 'Внешний код',
                'SORT' => 'Сортировка',
            ),
            'Анонс' =>
            array(
                'PREVIEW_PICTURE' => 'Картинка для анонса',
                'PREVIEW_TEXT' => 'Описание для анонса',
            ),
            'Подробно' =>
            array(
                'DETAIL_PICTURE' => 'Детальная картинка',
                'DETAIL_TEXT' => 'Детальное описание',
            ),
            'SEO' =>
            array(
                'IPROPERTY_TEMPLATES_ELEMENT_META_TITLE' => 'Шаблон META TITLE',
                'IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS' => 'Шаблон META KEYWORDS',
                'IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION' => 'Шаблон META DESCRIPTION',
                'IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE' => 'Заголовок элемента',
                'IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE' => 'Настройки для картинок анонса элементов',
                'IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT' => 'Шаблон ALT',
                'IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE' => 'Шаблон TITLE',
                'IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME' => 'Шаблон имени файла',
                'IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE' => 'Настройки для детальных картинок элементов',
                'IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT' => 'Шаблон ALT',
                'IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE' => 'Шаблон TITLE',
                'IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME' => 'Шаблон имени файла',
                'SEO_ADDITIONAL' => 'Дополнительно',
                'TAGS' => 'Теги',
            ),
            'Разделы' =>
            array(
                'SECTIONS' => 'Разделы',
            ),
        ));

        $helper->Iblock()->addElementIfNotExists($iblockId, array(
            'CODE' => 'office',
            'NAME' => 'Office',
                ), array(
            'ADDRESS' => '<p> Republic of Belarus, 220029, </ p> <p> Minsk, st. Krasnaya, 8 </ p>',
            'MARKER_TEXT_A' => 'st. Krasnaya, 8',
            'MAP' => '53.911412687363,27.57060939682'
        ));

        $helper->Iblock()->addElementIfNotExists($iblockId, array(
            'CODE' => 'lab',
            'NAME' => 'Laboratory',
                ), array(
            'ADDRESS' => '<p> Republic of Belarus, 220029, </ p> <p> Minsk, st. Krasnaya 7B </ p>',
            'MARKER_TEXT_A' => 'st. Krasnaya 7B',
            'MARKER_TEXT_B' => 'st. Krasnaya 8',
            'MAP' => '53.914367711784,27.572494711639',
            'WAY_POINT_WALKING' => array(
                '53.9140659813597,27.57346365472257',
                '53.913806284994095,27.572648263181996',
                '53.91247610750837,27.57051322480614',
                '53.91248877606256,27.56992313882286',
                '53.9122924129801,27.569665646757386',
                '53.91172865575689,27.570835089887854',
                '53.911912352762194,27.571114039625378'
            ),
            'WAY_POINT_DRIVING' => array(
                '53.914097651481796,27.57339928170564',
                '53.91346424337846,27.571854329313073',
                '53.9129333988774,27.572584951812594',
                '53.911875572318536,27.57112583010847'
            )
        ));
    }

    public function down() {
        $helper = new HelperManager();

        //your code ...
    }

}
