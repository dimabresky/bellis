<?php

namespace Sprint\Migration;

class FaqEn20170625165140 extends Version {

    protected $description = "FaqEn";

    public function up() {
        $helper = new HelperManager();

        $helper->Iblock()->addIblockTypeIfNotExists(array(
            'ID' => 'bellis',
            'SECTIONS' => 'Y',
            'EDIT_FILE_BEFORE' => '',
            'EDIT_FILE_AFTER' => '',
            'IN_RSS' => 'N',
            'SORT' => '500',
            'LANG' =>
            array(
                'ru' =>
                array(
                    'NAME' => 'Bellis',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ),
                'en' =>
                array(
                    'NAME' => 'Bellis',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ),
                'be' =>
                array(
                    'NAME' => 'Bellis',
                    'SECTION_NAME' => '',
                    'ELEMENT_NAME' => '',
                ),
            ),
        ));

        $iblockId = $helper->Iblock()->addIblockIfNotExists(array(
            'IBLOCK_TYPE_ID' => 'bellis',
            'LID' => 's7',
            'CODE' => 'Faq',
            'NAME' => 'Faq',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'LIST_PAGE_URL' => '#SITE_DIR#/faq/',
            'DETAIL_PAGE_URL' => '#SITE_DIR#/faq/',
            'SECTION_PAGE_URL' => '#SITE_DIR#/faq/',
            'PICTURE' => NULL,
            'DESCRIPTION' => '',
            'DESCRIPTION_TYPE' => 'text',
            'RSS_TTL' => '24',
            'RSS_ACTIVE' => 'Y',
            'RSS_FILE_ACTIVE' => 'N',
            'RSS_FILE_LIMIT' => NULL,
            'RSS_FILE_DAYS' => NULL,
            'RSS_YANDEX_ACTIVE' => 'N',
            'XML_ID' => '',
            'TMP_ID' => '883162eee80337fc11ea633632e7704f',
            'INDEX_ELEMENT' => 'Y',
            'INDEX_SECTION' => 'Y',
            'WORKFLOW' => 'N',
            'BIZPROC' => 'N',
            'SECTION_CHOOSER' => 'L',
            'LIST_MODE' => '',
            'RIGHTS_MODE' => 'S',
            'SECTION_PROPERTY' => 'N',
            'VERSION' => '2',
            'LAST_CONV_ELEMENT' => '0',
            'SOCNET_GROUP_ID' => NULL,
            'EDIT_FILE_BEFORE' => '',
            'EDIT_FILE_AFTER' => '',
            'SECTIONS_NAME' => 'Разделы',
            'SECTION_NAME' => 'Раздел',
            'ELEMENTS_NAME' => 'Элементы',
            'ELEMENT_NAME' => 'Элемент',
            'PROPERTY_INDEX' => 'N',
            'CANONICAL_PAGE_URL' => '',
            'EXTERNAL_ID' => '',
            'LANG_DIR' => '/',
            'SERVER_NAME' => 'bellis',
        ));

        $helper->Iblock()->updateIblockFields($iblockId, array(
            'IBLOCK_SECTION' =>
            array(
                'NAME' => 'Привязка к разделам',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' =>
                array(
                    'KEEP_IBLOCK_SECTION_ID' => 'N',
                ),
            ),
            'ACTIVE' =>
            array(
                'NAME' => 'Активность',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => 'Y',
            ),
            'ACTIVE_FROM' =>
            array(
                'NAME' => 'Начало активности',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'ACTIVE_TO' =>
            array(
                'NAME' => 'Окончание активности',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'SORT' =>
            array(
                'NAME' => 'Сортировка',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '0',
            ),
            'NAME' =>
            array(
                'NAME' => 'Название',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => '',
            ),
            'PREVIEW_PICTURE' =>
            array(
                'NAME' => 'Картинка для анонса',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' =>
                array(
                    'FROM_DETAIL' => 'N',
                    'SCALE' => 'N',
                    'WIDTH' => '',
                    'HEIGHT' => '',
                    'IGNORE_ERRORS' => 'N',
                    'METHOD' => 'resample',
                    'COMPRESSION' => 95,
                    'DELETE_WITH_DETAIL' => 'N',
                    'UPDATE_WITH_DETAIL' => 'N',
                    'USE_WATERMARK_TEXT' => 'N',
                    'WATERMARK_TEXT' => '',
                    'WATERMARK_TEXT_FONT' => '',
                    'WATERMARK_TEXT_COLOR' => '',
                    'WATERMARK_TEXT_SIZE' => '',
                    'WATERMARK_TEXT_POSITION' => 'tl',
                    'USE_WATERMARK_FILE' => 'N',
                    'WATERMARK_FILE' => '',
                    'WATERMARK_FILE_ALPHA' => '',
                    'WATERMARK_FILE_POSITION' => 'tl',
                    'WATERMARK_FILE_ORDER' => NULL,
                ),
            ),
            'PREVIEW_TEXT_TYPE' =>
            array(
                'NAME' => 'Тип описания для анонса',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => 'text',
            ),
            'PREVIEW_TEXT' =>
            array(
                'NAME' => 'Описание для анонса',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'DETAIL_PICTURE' =>
            array(
                'NAME' => 'Детальная картинка',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' =>
                array(
                    'SCALE' => 'N',
                    'WIDTH' => '',
                    'HEIGHT' => '',
                    'IGNORE_ERRORS' => 'N',
                    'METHOD' => 'resample',
                    'COMPRESSION' => 95,
                    'USE_WATERMARK_TEXT' => 'N',
                    'WATERMARK_TEXT' => '',
                    'WATERMARK_TEXT_FONT' => '',
                    'WATERMARK_TEXT_COLOR' => '',
                    'WATERMARK_TEXT_SIZE' => '',
                    'WATERMARK_TEXT_POSITION' => 'tl',
                    'USE_WATERMARK_FILE' => 'N',
                    'WATERMARK_FILE' => '',
                    'WATERMARK_FILE_ALPHA' => '',
                    'WATERMARK_FILE_POSITION' => 'tl',
                    'WATERMARK_FILE_ORDER' => NULL,
                ),
            ),
            'DETAIL_TEXT_TYPE' =>
            array(
                'NAME' => 'Тип детального описания',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => 'text',
            ),
            'DETAIL_TEXT' =>
            array(
                'NAME' => 'Детальное описание',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
            'XML_ID' =>
            array(
                'NAME' => 'Внешний код',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => '',
            ),
            'CODE' =>
            array(
                'NAME' => 'Символьный код',
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' =>
                array(
                    'UNIQUE' => 'Y',
                    'TRANSLITERATION' => 'Y',
                    'TRANS_LEN' => 100,
                    'TRANS_CASE' => 'L',
                    'TRANS_SPACE' => '-',
                    'TRANS_OTHER' => '-',
                    'TRANS_EAT' => 'Y',
                    'USE_GOOGLE' => 'N',
                ),
            ),
            'TAGS' =>
            array(
                'NAME' => 'Теги',
                'IS_REQUIRED' => 'N',
                'DEFAULT_VALUE' => '',
            ),
        ));




        $certification = $helper->Iblock()->addSectionIfNotExists($iblockId, array(
            'NAME' => 'Certification',
            'CODE' => 'sertifikatsiya',
        ));

        $declaration = $helper->Iblock()->addSectionIfNotExists($iblockId, array(
            'NAME' => 'Declaration',
            'CODE' => 'deklarirovanie',
        ));

        $helper->Iblock()->addElementIfNotExists($iblockId, array(
            'IBLOCK_SECTION_ID' => $certification,
            'NAME' => 'What expenses should be taken into account when maintaining and maintaining the QMS?',
            'CODE' => 'kakie-raskhody-nuzhno-uchest-pri-vedenii-podderzhanii-smk',
            'DETAIL_TEXT' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>'
        ));

        $helper->Iblock()->addElementIfNotExists($iblockId, array(
            'IBLOCK_SECTION_ID' => $certification,
            'NAME' => 'How to get a certificate?',
            'CODE' => 'kak-poluchit-sertifikat',
            'DETAIL_TEXT' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>'
        ));

        $helper->Iblock()->addElementIfNotExists($iblockId, array(
            'IBLOCK_SECTION_ID' => $declaration,
            'NAME' => 'Where can I find the text of the amendment No. 2 of the standard of the Republic of Belarus STB 2252-2012?',
            'CODE' => 'gde-mozhno-nayti-tekst-izmeneniya-2-standarta-respubliki-belarus-stb-2252-2012',
            'DETAIL_TEXT' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>'
        ));

        $helper->Iblock()->addElementIfNotExists($iblockId, array(
            'IBLOCK_SECTION_ID' => $declaration,
            'NAME' => 'I want to open a business for the sale of crystals from glass and semi-plastic from plastic, purchasing it in Russia. Do I need to certify the goods?',
            'CODE' => 'khochu-otkryt-ip-po-prodazhe-straz-iz-stekla-i-poluzhemchuga-iz-plastmassy-zakupaya-ego-v-rossii-nuzh',
            'DETAIL_TEXT' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>'
        ));
    }

    public function down() {
        $helper = new HelperManager();

        //your code ...
    }

}
