<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="popup-bg"></div>
    <div class="popup-content">
	    <div class="popup-intro popup-intro_vacancy">
	    	<div class="popup-close"></div>	    	
	    	<form action="" method="post" id="vacancy-form_popup" class="prevent" enctype="multipart/form-data">
                    <?=bitrix_sessid_post()?>                                                
                    <p class="popup-title"><?= GetMessage('MFT_APPLY_FOR_JOB')?></p>
<?if(!empty($arResult["ERROR_MESSAGE"]))
{?>
            <div class="mf-err-text" style="margin-bottom: 10px;">
<?	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);?>
            </div>
<?}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
        <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
                                                                    <input type="hidden" name="vacancy" value="" />
                                                                            <div class="faq-form_flex prevent">
                                                                                    <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_NAME"]?>" type="text" class="text-field" name="user_name" placeholder="<?=GetMessage("MFT_NAME")?>">
                                                                                    <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("LAST_NAME", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_LAST_NAME"]?>" type="text" class="text-field" name="user_last_name" placeholder="<?= GetMessage('MFT_LAST_NAME')?>">
                                                                            </div>
                                                                            <div class="faq-form_flex prevent">
                                                                                    <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_EMAIL"]?>" type="email" class="text-field" name="user_email" placeholder="<?=GetMessage("MFT_EMAIL")?>">
                                                                                    <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_PHONE"]?>" type="tel" class="text-field" name="user_phone" placeholder="<?= GetMessage('MFT_PHONE')?>">
                                                                            </div>
                                                                            <div class="prevent">
                                                                                <textarea <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> class="text-field_msg" name="MESSAGE" placeholder="<?= GetMessage('MFT_ABOUT_YOUR_SELF')?>"><?=$arResult["MESSAGE"]?></textarea>
                                                                            </div>
                        <?if($arParams["USE_CAPTCHA"] == "Y"):?>
                        <div class="mf-captcha">
                                <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
                                <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                                <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
                                <input class="text-field" type="text" name="captcha_word" size="30" maxlength="50" value="">
                        </div>
                        <?endif;?>
                                                                            <div class="btn-vacancy_intro prevent">
                                                                                    <button value="<?=GetMessage("MFT_SUBMIT")?>" name="submit" type="submit" class="btn btn-vacancy"><?=GetMessage("MFT_SUBMIT")?></button>
                                                                                 <div class="btn-vacancy_attach prevent">
                                                                                            <label for="attach-popup" class="text-field_attach prevent"><?= GetMessage("MFT_ATTACH_FILE")?>
                                                                                                    <input type="file" name="file_attachment" id="attach-popup" value="">
                                                                                                    <span class="attach-name"></span>
                                                                                            </label>												
                                                                                    </div>											
                                                                            </div>
			</form>
	    </div>
    </div>

<script>
if (typeof $ == 'function') {
    $(document).ready(function () {
        
        $('.popup-content .popup-close').click(function () {
            
            $('.mf-err-text, .mf-ok-text').remove();
        });
    });
}    
</script>
<?if ($_REQUEST['success'] == $arResult['PARAMS_HASH'] || !empty($arResult["ERROR_MESSAGE"])) {?>
<script>
if (typeof $ == 'function') {
    $('.popup-bg, .popup-content').addClass('show');
    $('body').addClass('hidden');
}
</script>
<?}?>