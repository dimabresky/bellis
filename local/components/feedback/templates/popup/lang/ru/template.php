<?
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_PHONE'] = "Телефон";
$MESS ['MFT_LAST_NAME'] = "Фамилия";
$MESS ['MFT_ABOUT_YOUR_SELF'] = "О себе";
$MESS ['MFT_APPLY_FOR_JOB'] = "Откликнуться на вакансию";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
$MESS ['MFT_ATTACH_FILE'] = "Прикрепить файл";

?>