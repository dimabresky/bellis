<?
$MESS ['MFT_TITLE'] = "Ask your question to a specialist";
$MESS ['MFT_NAME'] = "Name";
$MESS ['MFT_EMAIL'] = "Your E-mail";
$MESS ['MFT_PHONE'] = "Phone";
$MESS ['MFT_MESSAGE'] = "Message";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "Ask a question";
$MESS ['MFT_ATTACH_FILE'] = "Attach file";
$MESS ['MFT_VACANCY_TEXT'] = '<p class="faq-contacts_title">No vacancy needed?</p>
        <p class="faq-contacts_introtext">If you have not found a suitable vacancy, you can send us your resume. Our specialists will save your resume and will contact you as soon as a suitable vacancy opens</p>';
?>