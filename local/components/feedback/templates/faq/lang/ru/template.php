<?
$MESS ['MFT_TITLE'] = "Задайте свой вопрос специалисту";
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_PHONE'] = "Телефон";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Задайте вопрос";
$MESS ['MFT_ATTACH_FILE'] = "Прикрепить файл";