<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="faq-contacts">
        <p class="faq-contacts_title"><?= GetMessage('MFT_TITLE')?></p>
        <form action="<?=POST_FORM_ACTION_URI?>" method="post" id="faq-form">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{?>
            <div style="margin-bottom: 10px;">
<?	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);?>
            </div>
<?}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}?>
            <?= bitrix_sessid_post()?>
            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
                <div class="faq-form_flex">
                        <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_NAME"]?>" type="text" class="text-field" name="user_name" placeholder="<?=GetMessage("MFT_NAME")?>" />
                        <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_EMAIL"]?>" type="email" class="text-field" name="user_email" placeholder="<?=GetMessage("MFT_EMAIL")?>" />
                        <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_PHONE"]?>" type="tel" class="text-field" name="user_phone" placeholder="<?= GetMessage('MFT_PHONE')?>">
                </div>
                <?if($arParams["USE_CAPTCHA"] == "Y"):?>
                        <div class="mf-captcha">
                                <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
                                <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                                <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
                                <input class="text-field" type="text" name="captcha_word" size="30" maxlength="50" value="">
                        </div>
                <?endif;?>
                <div>
                        <textarea <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> class="text-field_msg" name="MESSAGE"><?=$arResult["MESSAGE"]?></textarea>
                </div>
                <div>
                        <button name="submit" value="<?= GetMessage('MFT_SUBMIT')?>" type="submit" class="btn btn-faq"><?= GetMessage('MFT_SUBMIT')?></button>
                </div>
        </form>
</div>
</div> <!-- end of .content-inner_faq -->
<?if ($_REQUEST['success'] == $arResult['PARAMS_HASH'] || !empty($arResult["ERROR_MESSAGE"])) {?>
<script>
if (typeof $ == 'function') {
    $('html, body').animate({
        scrollTop: $(".scroll-to-form").offset().top
    }, 1000);
}
</script>
<?}?>