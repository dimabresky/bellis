<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="vacancy-feedback faq-contacts scroll-to-form">
        <?= GetMessage('MFT_VACANCY_TEXT')?>
        <form action="<?=POST_FORM_ACTION_URI?>" method="post" id="vacancy-form" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{?>
            <div style="margin-bottom: 10px;">
<?	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);?>
            </div>
<?}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
                        <div class="faq-form_flex">
                            <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_NAME"]?>" type="text" class="text-field" name="user_name" placeholder="<?=GetMessage("MFT_NAME")?>">
                            <input <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> value="<?=$arResult["AUTHOR_EMAIL"]?>" type="email" class="text-field" name="user_email" placeholder="<?=GetMessage("MFT_EMAIL")?>">
                        </div>
                        <div>
                            <textarea <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>required=""<?endif?> class="text-field_msg" name="MESSAGE" placeholder="<?=GetMessage("MFT_MESSAGE")?>"><?=$arResult["MESSAGE"]?></textarea>
                        </div>
                        <?if($arParams["USE_CAPTCHA"] == "Y"):?>
                        <div class="mf-captcha">
                                <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
                                <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                                <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
                                <input class="text-field" type="text" name="captcha_word" size="30" maxlength="50" value="">
                        </div>
                        <?endif;?>
                        <div class="btn-vacancy_intro">
                             <div class="btn-vacancy_attach">
                                 <label for="attach" class="text-field_attach"><?= GetMessage("MFT_ATTACH_FILE")?>
                                                <input type="file" name="file_attachment" id="attach" value="">
                                                <span class="attach-name"></span>
                                        </label>												
                                </div>
                                <button value="<?=GetMessage("MFT_SUBMIT")?>" name="submit" type="submit" class="btn btn-vacancy"><?=GetMessage("MFT_SUBMIT")?></button>
                        </div>
                </form>
</div>
<?if ($_REQUEST['success'] == $arResult['PARAMS_HASH']  || !empty($arResult["ERROR_MESSAGE"])) {?>
<script>
if (typeof $ == 'function') {
    $('html, body').animate({
        scrollTop: $(".scroll-to-form").offset().top
    }, 1000);
}
</script>
<?}?>