<?
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
$MESS ['MFT_ATTACH_FILE'] = "Прикрепить файл";
$MESS ['MFT_VACANCY_TEXT'] = '<p class="faq-contacts_title">Нет нужной вакансии?</p>
        <p class="faq-contacts_introtext">Если вы не нашли подходящую вакансию, вы можете выслать нам ваше резюме. Наши специалисты сохранаят ваше резюме и свяжутся с вами как только откроется подходяшая вакансия</p>';
?>