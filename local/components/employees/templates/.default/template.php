<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(true);

if (!$arResult['DEPARTMENTS']) {
    return;
}

//\bellis\dm($arResult);

if (!function_exists('getEmployeesHtml')) {
    
    /**
     * @param array $arEmployeesList
     * @param array $arEmployees
     * @param string $code
     * @param bool $active
     * @return string
     */
    function getEmployeesHtml($arEmployeesList, $arEmployees, $code, $active, $class_main, $class_active) {

        $employees = '<div class="' . $class_main . ($active ? ' ' . $class_active : '') . '" id="' . $code . '">';

        $employees .= '<ul class="workers-list">';

        foreach ($arEmployeesList as $empId) {

            $employees .= '<li>';

            $arAvatar = CFile::ResizeImageGet(
                            $arEmployees[$empId]['DETAIL_PICTURE'], array('width' => 120, 'height' => 150), BX_RESIZE_IMAGE_EXACT
            );

            $name = $arEmployees[$empId]['NAME'];

            $employees .= '<div class="workers-thumb"><img src="' . $arAvatar['src'] . '" alt="' . $name . '"></div>';

            $employees .= '<div class="workers-intro">';

            if (($position = $arEmployees[$empId]['POSITION'])) {

                $employees .= '<p class="worker-position">' . $position . '</p>';
            }

            $employees .= '<p class="worker-name">' . $name . '</p>';

            if ($arEmployees[$empId]['PROPERTIES']['PHONE']['VALUE']) {

                $employees .= '<div class="worker-phone">';
                $employees .= '<p><a href="tel:' . $arEmployees[$empId]['PROPERTIES']['PHONE']['VALUE'] . '">' . $arEmployees[$empId]['PROPERTIES']['PHONE']['VALUE'] . '</a></p>';
                $employees .= '</div>';
            }

            if ($arEmployees[$empId]['PROPERTIES']['EMAIL']['VALUE']) {

                $employees .= '<div class="worker-email">';
                $employees .= '<p><a href="mailto:' . $arEmployees[$empId]['PROPERTIES']['EMAIL']['VALUE'] . '">' . $arEmployees[$empId]['PROPERTIES']['EMAIL']['VALUE'] . '</a></p>';
                $employees .= '</div>';
            }

            if ($arEmployees[$empId]['PROPERTIES']['SKYPE']['VALUE']) {

                $employees .= '<div class="worker-skype">';
                $employees .= '<p><a href="skype:' . $arEmployees[$empId]['PROPERTIES']['SKYPE']['VALUE'] . '?chat">' . $arEmployees[$empId]['PROPERTIES']['SKYPE']['VALUE'] . '</a></p>';
                $employees .= '</div>';
            }

            $employees .= '</div>';

            $employees .= '</li>';
        }

        $employees .= '</ul>';

        $employees .= '</div>';

        return $employees;
    }

}
$active = true;
foreach ($arResult['DEPARTMENTS_TREE'] as $parentDepId => $subDeps):

    $content = $employees = $subLi = '';

    if ($subDeps) {

        $sub_active = true;
        foreach ($subDeps as $depId) {

            $subLi .= '<li ' . ($sub_active ? 'class="active"' : '') . '><a href="#' . $arResult['DEPARTMENTS'][$depId]['CODE'] . '">' . $arResult['DEPARTMENTS'][$depId]['NAME'] . '</a></li>';

            if ($arResult['EMPLOYEES_GROUP_BY_DEPARTMENTS'][$depId]) {
                
                $employees .= getEmployeesHtml($arResult['EMPLOYEES_GROUP_BY_DEPARTMENTS'][$depId], $arResult['EMPLOYEES'], $arResult['DEPARTMENTS'][$depId]['CODE'], $sub_active, 'contacts-tabs_content', 'active');
            }

            $sub_active = false;
        }
        
    } elseif ($arResult['EMPLOYEES_GROUP_BY_DEPARTMENTS'][$parentDepId]) {
        
        $employees = getEmployeesHtml($arResult['EMPLOYEES_GROUP_BY_DEPARTMENTS'][$parentDepId], $arResult['EMPLOYEES'], $arResult['DEPARTMENTS'][$parentDepId]['CODE'], $active, 'workers-list_intro', 'show');
    }

    if (strlen($subLi) > 0) {

        $content .= '<div class="workers-list_intro '.($active ? 'show' : '').'" ><ul class="contacts-nav">' . $subLi . '</ul>' . $employees . "</div>";
    } else {

        $content .= $employees;
    }

    if (strlen($content) > 0):
        
        ?>
        <div class="content-inner_contacts" id="<?= $arResult['DEPARTMENTS'][$parentDepId]['CODE'] ?>">
            <p class="contacts-title contacts-title_toggle"><?= $arResult['DEPARTMENTS'][$parentDepId]['NAME'] ?></p>
        <?= $content ?>
        </div>

        <? endif ?>
    <? $active = false; endforeach ?>
