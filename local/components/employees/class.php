<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

class BellisEmployees extends CBitrixComponent {
    
    /**
     * Create departments tree
     */
    protected function _createDepartmentsTree () {
        
        foreach ($this->arResult['DEPARTMENTS'] as $arDepartment) {
            
            if ($arDepartment['DEPTH_LEVEL'] > 1) {
                continue;
            }
            
            $this->arResult['DEPARTMENTS_TREE'][$arDepartment['ID']] = null;
            
            foreach ($this->arResult['DEPARTMENTS'] as $arrDepartment) {
                
                if ($arrDepartment['IBLOCK_SECTION_ID'] == $arDepartment['ID']) {
                    
                    $this->arResult['DEPARTMENTS_TREE'][$arDepartment['ID']][] = $arrDepartment['ID'];
                    
                }
                
            }
            
        }
        
    }
    
    /**
     * Create employees group departments
     */
    protected function _createEmployeesGroupByDepartments () {
        
        $dbEmployeesGroups = CIBlockElement::GetElementGroups(array_keys($this->arResult['EMPLOYEES']), true);
        
        while ($arEmployee = $dbEmployeesGroups->GetNext()) {
            $this->arResult['EMPLOYEES_GROUP_BY_DEPARTMENTS'][$arEmployee['ID']][] = $arEmployee['IBLOCK_ELEMENT_ID'];
        }
        
    }


    /**
     * Executing the component
     * @global \CCacheManager $CACHE_MANAGER
     * @throws \Exception
     */
    public function executeComponent() {

        try {

            if (!\Bitrix\Main\Loader::includeModule('iblock')) {

                throw new \Exception('Iblock module not found');
            }

            $this->arParams['IBLOCK_ID'] = (int) $this->arParams['IBLOCK_ID'];

            if ($this->arParams['IBLOCK_ID'] < 0) {

                throw new \Exception('Enter correct IBLOCK_ID');
            }

            $arFilter = array(
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 
                'ACTIVE' => 'Y',
                '<=DEPTH_LEVEL' => 2
            );

            $obCache = new CPHPCache();
            
            if ($obCache->InitCache(3600, serialize($arFilter) . serialize($this->arParams), "/iblock/employees")) {

                $this->arResult = $obCache->GetVars();
            } elseif ($obCache->StartDataCache()) {
                
                $dbDepartments = CIBlockSection::GetList(
                        
                            array('SORT' => 'ASC'),
                            $arFilter,
                            false,
                            array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'CODE', 'DEPTH_LEVEL')
                            
                        );
                
                while ($arDepartment = $dbDepartments->GetNext()) {
                    
                    $this->arResult['DEPARTMENTS'][$arDepartment['ID']] = $arDepartment;
                }
                
                $this->_createDepartmentsTree();
                
                $dbEmployees = CIBlockElement::GetList(
                        
                            array('SORT' => 'ASC'),
                            $arFilter,
                            false, false
                        
                        );
                
                while ($dbEmployee = $dbEmployees->GetNextElement()) {
                    
                    $arFields = $dbEmployee->GetFields();
                    
                    $this->arResult['EMPLOYEES'][$arFields['ID']] = $arFields;
                    $this->arResult['EMPLOYEES'][$arFields['ID']]['PROPERTIES'] = $dbEmployee->GetProperties();
                }
                
                $this->_createEmployeesGroupByDepartments();
                
                if (defined("BX_COMP_MANAGED_CACHE")) {

                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache("/iblock/employees");
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . $this->arParams['IBLOCK_ID']);
                    $CACHE_MANAGER->EndTagCache();
                }

                $obCache->EndDataCache($this->arResult);
            }
            
            $this->IncludeComponentTemplate();
            
        } catch (\Exception $e) {

            ShowError($e->getMessage());
        }
    }

}
