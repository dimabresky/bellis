<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

Bitrix\Main\Loader::includeModule('iblock');
$dbIblocks = CIBlock::GetList(array('ID' => 'DESC'), array('TYPE' => 'bellis'));
while ($res = $dbIblocks->Fetch()) {

    $arIblocks[$res['ID']] = $res['NAME'];
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_ID" => Array(
            "NAME" => "Инфоблок хранения списка сотрудников",
            "TYPE" => "LIST",
            "VALUES" => $arIblocks,
            "DEFAULT" => "",
            "MULTIPLE" => "N",
            "PARENT" => "BASE",
        ),
        "CACHE_TIME" => array("DEFAULT" => 36000000)
    )
);
