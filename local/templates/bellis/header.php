<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/const.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/include/functions.php';


// LOCAL REDIRECT
if (!$USER->IsAdmin()) {
    
    $dirs = array(
        SITE_DIR . 'certification/',
        SITE_DIR . 'declaration/',
        SITE_DIR . 'ispitaniya/',
        SITE_DIR . 'international-cooperation/',
        SITE_DIR . 'energy-efficiency/',
        SITE_DIR . 'markets/',
        SITE_DIR . 'the-belarusian-market/',
        SITE_DIR . 'international-markets/'
    );
    
    $currentDir = $APPLICATION->GetCurDir();
    
    if (in_array($currentDir, $dirs) &&
            file_exists($_SERVER['DOCUMENT_ROOT'] . $currentDir . ".inner.menu.php")) {
        
        require $_SERVER['DOCUMENT_ROOT'] . $currentDir . ".inner.menu.php";
        
        if ($aMenuLinks) {
            LocalRedirect($aMenuLinks[0][1]);
        }
        
    }
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<!DOCTYPE>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <? $APPLICATION->ShowHead() ?>
        <title><?= $APPLICATION->ShowTitle() ?></title>
        <?
        $APPLICATION->AddHeadString('<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic-ext" rel="stylesheet">');
        $APPLICATION->AddHeadString('<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">');
        $APPLICATION->AddHeadString('<meta name="format-detection" content="telephone=no">');
        $APPLICATION->AddHeadString('<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />');
        $APPLICATION->AddHeadString('<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">');

        $asset = Bitrix\Main\Page\Asset::getInstance();
        $asset->addCss(SITE_TEMPLATE_PATH . '/css/reset.css');
        $asset->addCss(SITE_TEMPLATE_PATH . '/css/main.css');
        $asset->addCss(SITE_TEMPLATE_PATH . '/css/jquery.mmenu.all.css');

        $asset->addJs(SITE_TEMPLATE_PATH . '/js/jquery.2.1.4.js');
        $asset->addJs(SITE_TEMPLATE_PATH . '/js/jquery.mmenu.all.min.js');
        $asset->addJs(SITE_TEMPLATE_PATH . '/js/jquery.mCustomScrollbar.min.js');
        $asset->addJs(SITE_TEMPLATE_PATH . '/js/jquery.mousewheel.min.js');
        $asset->addJs(SITE_TEMPLATE_PATH . '/js/common.js');
        ?>

    </head>
    <body>
        <div class="blur-wrap" id="sb-site">
            <div class="sb-site_left">
                <div class="mobile-nav_btn" id="mmenu-icon"></div>
                <div class="mobile-nav_close"></div>
                <div class="search-btn_mobile"><div class="search-btn"></div></div>
                <div class="logo-block">
                    <a href="<?= SITE_DIR ?>"></a>
                </div>					
            </div>
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:menu", "main.left", array(
                "ROOT_MENU_TYPE" => "left",
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "COMPONENT_TEMPLATE" => "main.left"
                    ), false
            );
            ?>

            <div class="sb-site_right">
                <header>
                    <div class="container">
                        <div class="lang-nav">
                            <ul class="lang-nav_list">
                                <? if (LANGUAGE_ID == 'ru'): ?>
                                    <li class="current">ru</li>
                                <? else: ?>
                                    <li><a href="/">ru</a></li>
                                <? endif ?>
                                <? if (LANGUAGE_ID == 'en'): ?>
                                    <li class="current">en</li>
                                <? else: ?>
                                    <li><a href="/en/">en</a></li>
                                <? endif ?>

                            </ul>
                            <?
                            $APPLICATION->IncludeComponent(
                                    "bitrix:search.title", "", Array(
                                "CATEGORY_0" => array("main", "iblock_bellis"),
                                "CATEGORY_0_TITLE" => "",
                                "CATEGORY_0_iblock_bellis" => array("all"),
                                "CATEGORY_0_main" => array(""),
                                "CHECK_DATES" => "N",
                                "CONTAINER_ID" => "title-search",
                                "INPUT_ID" => "title-search-input",
                                "NUM_CATEGORIES" => "1",
                                "ORDER" => "date",
                                "PAGE" => "#SITE_DIR#search/index.php",
                                "SHOW_INPUT" => "Y",
                                "SHOW_OTHERS" => "N",
                                "TOP_COUNT" => "5",
                                "USE_LANGUAGE_GUESS" => "Y"
                                    )
                            );
                            ?>
                        </div>
                        <?
                        $APPLICATION->IncludeComponent(
                                "bitrix:menu", "main.top", array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "2",
                            "CHILD_MENU_TYPE" => "inner",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "COMPONENT_TEMPLATE" => "main.top"
                                ), false
                        );
                        ?>



                    </div>
                </header>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "img_block",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "",
                    "COMPONENT_TEMPLATE" => ".default"
                        ), false
                );
                ?>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "page",
                    "AREA_FILE_SUFFIX" => "cells_block",
                    "AREA_FILE_RECURSIVE" => "N",
                    "EDIT_TEMPLATE" => "",
                    "COMPONENT_TEMPLATE" => ".default"
                        ), false
                );
                ?>
                <div class="content-inner">
                    <?
                    $class = '';
                    if (CSite::InDir(SITE_DIR . 'contacts/')) {

                        $class1 = 'container_workers';
                        $class2 = 'content-inner_workers';
                    }
                    ?>
                    <div class="container <?= $class1 ?>">
                        <?
                        $APPLICATION->IncludeComponent(
                                "bitrix:menu", "inner.right", array(
                            "ROOT_MENU_TYPE" => "inner",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "inner",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "COMPONENT_TEMPLATE" => "inner.right"
                                ), false
                        );
                        ?>
                        <div class="content-inner_text <?= $class2?>">
                            <? if ($APPLICATION->GetDirProperty('IS_STATIC_PAGE') == 'Y'): ?>
                                <div class="text-page">
                                <? endif; ?>