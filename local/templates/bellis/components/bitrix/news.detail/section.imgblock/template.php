<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
if ($arResult['ID'] <= 0) {
    return;
}

$arImg = null;
if ($arResult['PROPERTIES']['IMG']['VALUE'] > 0) {

    $arImg = CFile::GetFileArray($arResult['PROPERTIES']['IMG']['VALUE']);
}

if ($arResult['PROPERTIES']['SUBSECTION']['VALUE'] != "Y") {
    ?>
    <div class="main-index" <? if ($arImg['SRC']): ?>style="background: url(<?= $arImg['SRC'] ?>) center center no-repeat; background-size: cover;"<? endif ?>>
        <div class="main-index_overlay">
            <div class="container">
                <div class="main-index_intro">
                    <div class="main-index_cell">
                        <h1 class="main-index_title"><? $APPLICATION->ShowTitle(false) ?></h1>
    <? if (strlen($arResult['PROPERTIES']['SUB_TITLE' . POSTFIX_PROPERTY]['VALUE']) > 0): ?>
                            <p class="main-index_introtext"><?= $arResult['PROPERTIES']['SUB_TITLE' . POSTFIX_PROPERTY]['VALUE'] ?></p>
    <? endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? } else { ?>

    <div class="pagetitle-intro" style="background: url(<?= $arImg['SRC'] ?>) center center no-repeat;background-size: cover;">
        <div class="pagetitle-intro_overlay">
            <div class="container">
                <h1 class="pagetitle"><? $APPLICATION->ShowTitle(false) ?></h1>
                <? if (strlen($arResult['PROPERTIES']['SUB_TITLE' . POSTFIX_PROPERTY]['VALUE']) > 0): ?>
                    <p class="main-index_introtext"><?= $arResult['PROPERTIES']['SUB_TITLE' . POSTFIX_PROPERTY]['VALUE'] ?></p>
                <? endif ?>
                <?
                $APPLICATION->IncludeComponent("bitrix:breadcrumb", "bellisbreadcrumb", Array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => SITE_ID
                        )
                );
                ?>

            </div>
        </div>
    </div>

<?
}