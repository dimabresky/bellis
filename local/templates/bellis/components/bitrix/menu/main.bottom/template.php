<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="footer-nav">						
    <ul class="footer-nav_flex">
<?
$previousLevel = 0;
foreach($arResult as $arItem):
    if ($arItem['PARAMS']['add_class']) {
        $arItem["IS_PARENT"] = false;
        $previousLevel = $arItem["DEPTH_LEVEL"];
    }
    ?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li><p class="itemtitle"><a href="javascript:void(0)" ><?=$arItem["TEXT"]?></a></p>
				
		<?else:?>
			<li><p class="itemtitle"><a href="javascript:void(0)" ><?=$arItem["TEXT"]?></a></p>
				
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <p <?if ($arItem['PARAMS']['add_class']):?>class="<?= $arItem['PARAMS']['add_class']?>"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></p>
			<?else:?>
				<p <?if ($arItem['PARAMS']['add_class']):?>class="<?= $arItem['PARAMS']['add_class']?>"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></p>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <p <?if ($arItem['PARAMS']['add_class']):?>class="<?= $arItem['PARAMS']['add_class']?>"<?endif?>><a href="javascript:void(0)"><?=$arItem["TEXT"]?></a></p>
				
			<?else:?>
				<p <?if ($arItem['PARAMS']['add_class']):?>class="<?= $arItem['PARAMS']['add_class']?>"<?endif?>><a href="javascript:void(0)"><?=$arItem["TEXT"]?></a></p>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</li>", ($previousLevel-1) );?>
<?endif?>
    </ul>
</div>