<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<?
        $id1 = $id2 = $script = '';
        if (CSite::InDir(SITE_DIR. 'contacts/')) {
            
            $id1 = 'id="scrollbar"';
            $id2 = 'id="scrollbar_list"';
            $this->addExternalJs(SITE_TEMPLATE_PATH . '/js/scrollto.js');
            $script = '<script text="text/javascript" src="' . SITE_TEMPLATE_PATH . '/js/sticky.js' .'"></script>';
        }
?>

<div class="content-inner_right" <?= $id1?>>
    <div class="content-inner_nav">
        <?$APPLICATION->ShowViewContent('active_point')?>
        
        <ul class="inner-nav" <?= $id2?>>
<?
foreach($arResult as $arItem):?>
    <li>
        <a class="<?if ($arItem['SELECTED']) {echo 'active'; $APPLICATION->AddViewContent('active_point', '<p class="inner-nav_title"  id="scrollbar_title">'.$arItem['TEXT'].'</p>');}?>" href="<?if ($arItem["PERMISSION"] > "D") {echo $arItem['LINK'];} else {echo 'javasctip:void(0)';}?>">
            <?= $arItem['TEXT']?>
        </a>
    </li>
<?endforeach?>
    </ul>				
    </div>	
</div>	
<?endif?>
<?= $script?>