<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="icons-block">
    <ul class="icons-block_list">
<?
foreach($arResult as $arItem):?>
    <li>
        <a href="<?if ($arItem["PERMISSION"] > "D") {echo $arItem['LINK'];} else {echo 'javasctip:void(0)';}?>">
            <span class="icons-block_cell"><?if ($arItem['PARAMS']['svg']) { echo $arItem['PARAMS']['svg']; }?></span>
            <span class="icons-block_title">
                    <?= $arItem['TEXT']?>
            </span>
        </a>
    </li>
<?endforeach?>
    </ul>				
</div>	
<?endif?>