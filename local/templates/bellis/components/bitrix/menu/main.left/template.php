<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="fixed">				
    <ul class="main-nav">
<?
foreach($arResult as $arItem):?>
    <li class="<?= $arItem['PARAMS']['class']?> <?if ($arItem['SELECTED']) {echo 'active';}?>">
        <a href="<?if ($arItem["PERMISSION"] > "D") {echo $arItem['LINK'];} else {echo 'javasctip:void(0)';}?>">
            <span class="main-nav_icon">
            <?if ($arItem['PARAMS']['svg']) { echo $arItem['PARAMS']['svg']; }?>
            </span>
            <span class="main-nav_title"><?= $arItem['TEXT']?></span>
        </a>
    </li>
<?endforeach?>
    </ul>				
</nav>	
<?endif?>
				