<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (empty($arResult["ALL_ITEMS"]))
    return;

?>
<style>
    
    a.delete-section-img:before {
        
        background: none !important;
    }
    
    a.delete-section-img {
        
        padding-left: 0px !important;
    }
</style>
<ul class="base-nav desktop-filters">
<? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>
    <? if (is_array($arColumns) && count($arColumns) > 0): ?>
            <li class="base-sub">
                <input <? if ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]): ?> checked=""<? endif ?> type="checkbox" name ="group-<?= $itemID ?>" id="group-<?= $itemID ?>">
                <label for="group-<?= $itemID ?>"><a class="delete-section-img" href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemID]['PARAMS']['title']?></a></label>
                    <? foreach ($arColumns as $key => $arRow): ?>
                    <ul<? if ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]): ?> class="show"<? endif ?>>
            <? foreach ($arRow as $itemIdLevel_2 => $arLevel_3): ?>
                <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?>
                                <li class="base-sub">
                                    <input <? if ($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]): ?> checked=""<? endif ?> type="checkbox" name ="sub-group-<?= $itemIdLevel_2 ?>" id="sub-group-<?= $itemIdLevel_2 ?>">
                                    <label for="sub-group-<?= $itemIdLevel_2 ?>"><a class="delete-section-img" href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemIdLevel_2]['PARAMS']['title']?></a></label>
                                    <ul<? if ($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]): ?> class="show"<? endif ?>>
                                        <? foreach ($arLevel_3 as $itemIdLevel_3): ?>
                                            <li><a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]['title']?></a></li>
                                <? endforeach ?>
                                    </ul>
                                </li>
                            <? else: ?>
                                <li><a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]['title']?></a></li>
                        <? endif ?>
                    <? endforeach; ?>
                    </ul>
            <? endforeach ?>
            </li>
        <? else: ?>
            <li><a href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemID]["PARAMS"]['title']?></a></li>
    <? endif ?>
<? endforeach ?>
</ul>