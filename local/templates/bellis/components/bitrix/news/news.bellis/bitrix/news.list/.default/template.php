<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="content-inner_news">
    <div class="news-intro">
        <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?><br />
        <? endif; ?>
        <ul class="news-block_list">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                $arImg = null;
                if ($arItem['PREVIEW_PICTURE']['ID'] > 0) {
                    $arImg = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 475, 'height' => 357), BX_RESIZE_IMAGE_EXACT);
                }
                ?>
                <li>
                    <div class="news-thumb"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arImg['src'] ?>" alt="<?= $arItem['NAME'] ?>" /></a></div>
                    <div class="news-content">
                        <p class="news-content_date"><?= \bellis\formateDateByLang($arItem['ACTIVE_FROM']) ?></p>
                        <p class="news-content_title"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></p>
                        <p class="news-content_introtext"><?= $arItem['PREVIEW_TEXT'] ?></p>
                    </div>
                </li>
            <? endforeach; ?>
        </ul>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
        <? endif; ?>
    </div>
</div>
