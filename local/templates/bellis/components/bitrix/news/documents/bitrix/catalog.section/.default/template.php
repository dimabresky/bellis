<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult['ITEMS'])) {

    return;
}

$arClasses = array(
    'doc' => 'doc',
    'docx' => 'doc',
    'txt' => 'doc',
    'xls' => 'ext',
    'xlsx' => 'ext',
    'pdf' => 'pdf',
);
?>
<div class="base-inner_text">
    <ul class="base-results_list">
        <?
        foreach ($arResult['ITEMS'] as $arItem):

            $file = $arItem['DISPLAY_PROPERTIES']['FILE']['FILE_VALUE'];
            $arFileParts = explode('.', $file['ORIGINAL_NAME']);
            $ext = array_pop($arFileParts);
            ?>
            <li><a class="<?= $arClasses[$ext] ?>" target="__blank" href="<?= $file['SRC'] ?>"><?= $arItem['NAME'] ?></a></li>
<? endforeach ?>
    </ul>
</div>



