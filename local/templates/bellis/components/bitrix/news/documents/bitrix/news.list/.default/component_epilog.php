<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (LANGUAGE_ID != 'ru') {
    
    $obCache = new CPHPCache();
    if ($obCache->InitCache(3600, serialize(array($arParams['IBLOCK_ID'], $arResult['ID'])), "/iblock/breadcrumbs")) {
        
        $arrSections = $obCache->GetVars();
    } elseif ($obCache->StartDataCache()) {
        
        $dbSections = CIBlockSection::GetNavChain(
                    $arParams['IBLOCK_ID'], $arResult['ID'], array('ID'));
    
        while ($arSection = $dbSections->Fetch()) {

            $arrSection[] = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arSection['ID']), false,
                array('ID', 'SECTION_PAGE_URL'))->GetNext();
        }
        
        if (defined("BX_COMP_MANAGED_CACHE")) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/breadcrumbs");
            $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);
            $CACHE_MANAGER->EndTagCache();
        }
        
        $obCache->EndDataCache($arrSection);
    }
    
    foreach ($arrSection as $arSec) {
        
        $APPLICATION->AddChainItem($arSec['NAME'], $arSec['SECTION_PAGE_URL']);
    }
}
