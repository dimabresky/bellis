<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (empty($arResult['ITEMS'])) {
    return;
}

?>
<ul class="vacancy-list">
<?
foreach ($arResult['ITEMS'] as $arItem):
?>
    <li>
            <div class="vacancy-list_title">										
                <p class="vacancy-list_position"><?= $arItem['NAME']?></p>
                    <p class="vacancy-list_salary"><?= $arItem['PROPERTIES']['ZP']['VALUE']?></p>
            </div>
            <div class="vacancy-list_info">
                    <?= $arItem['DETAIL_TEXT']?>
                    <div class="vacancy-list_btn btn-faq"><?= GetMessage('SHOW_FORM_BTN')?></div>
            </div>
    </li>
<? endforeach; ?>
</ul>