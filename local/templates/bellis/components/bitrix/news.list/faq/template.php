<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 

$this->setFrameMode(true);
?>

<?
if (empty($arResult['ITEMS'])) {
    return;
}
?>

<div class="content-inner_faq">
    <div class="faq_search">									
        <form action="" method="get" class="search-form" id="search_form_faq">
            <input name="" class="search_query" value="" placeholder="<?= GetMessage("FORM_SEARCH_PLACEHOLDER") ?>" />
            <button type="submit" class="btn btn-search"></button>
            <input type="reset" class="btn-reset" value="" />										
        </form>
    </div>
    <?
    $arSectionsId = array_map(function ($arItem) {
        return $arItem['IBLOCK_SECTION_ID'];
    }, $arResult['ITEMS']);

    $dbSections = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arSectionsId, 'ACTIVE' => 'Y'), false, array('ID', 'NAME', 'UF_NAME' . POSTFIX_PROPERTY));

    while ($arRes = $dbSections->Fetch()):
        $sectionId = $arRes['ID'];
        $arItems = array_filter($arResult['ITEMS'], function ($arItem) use ($sectionId) {
            return $sectionId == $arItem['IBLOCK_SECTION_ID'];
        });
        if (empty($arItems)) {
            continue;
        }
        ?>
        <div class="faq-intro">
            <p class="faq-intro_title"><span><?= $arRes['NAME'] ?></span></p>
            <ul class="faq-list">
                <? foreach ($arItems as $arItem) : ?>
                    <li>
                        <p class="faq-title"><span><?= $arItem['NAME'] ?></span></p>
                        <div class="faq-answer">
                            <?= $arItem['DETAIL_TEXT'] ?>
                        </div>
                    </li>
                <? endforeach ?>
            </ul>
        </div>
    <? endwhile; ?>
