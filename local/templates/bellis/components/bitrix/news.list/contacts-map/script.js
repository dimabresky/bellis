
$(document).ready(function () {

    /**
     * 
     * @param {String} mapId
     * @param {Array} center
     * @param {Number} zoom
     * @returns {ymaps.Map}
     */
    function initMap(mapId, center, zoom) {

        return new ymaps.Map(mapId, {
            center: center,
            zoom: zoom || 15,
            controls: ["zoomControl", "fullscreenControl"]
        });

    }

    /**
     * 
     * @param {ymaps.Map} map
     * @param {Array} position
     * @param {String} content
     * @returns {undefined}
     */
    function drawMarker(map, position, content) {

        map.geoObjects.add(new ymaps.Placemark(position, {hintContent: content, balloonContent: content}));

    }

    /**
     * 
     * @param {google.maps.Map} map
     * @param {Array} way_points
     * @param {String} color
     * @param {String} contentPlacemarkA
     * @param {String} contentPlacemarkB
     * @returns {undefined}
     */
    function drawRoute(map, way_points, color, contentPlacemarkA, contentPlacemarkB) {
        
        map.geoObjects.removeAll();
        map.geoObjects.add(new ymaps.Polyline(way_points, {}, {strokeColor: color, strokeWidth: 3}));
        drawMarker(map, way_points[0], contentPlacemarkA);
        drawMarker(map, way_points[way_points.length - 1], contentPlacemarkB);
    }

    /**
     * 
     * @param {$} $mapArea
     * @returns {undefined}
     */
    function initMapWork($mapArea) {

        var $btnWalk;

        var $btnDrive;

        var point = $mapArea.data('point');
        
        var map = initMap($mapArea.attr('id'), point.center || point.way_points_walking[0] || point.way_points_driving[0], 15);
        
        if (typeof point.way_points_walking === 'undefined' && typeof point.way_points_driving === 'undefined') {
            drawMarker(map, point.center, point.contentPlacemarkA);
            return;
        }
        
        if (typeof point.way_points_walking !== 'undefined') {

            $btnWalk = $mapArea.siblings('.btn-map_intro').find('.btn-map_walk');

            if ($btnWalk.hasClass('active')) {
                drawRoute(map, point.way_points_walking, '#FF0000', point.contentPlacemarkA, point.contentPlacemarkB);
            }

            $btnWalk.on('click', function () {

                var $this = $(this);

                if (!$this.hasClass('active')) {

                    $this.parent().find('.ctrl-btn').removeClass('active');
                    $this.addClass('active');

                    drawRoute(map, point.way_points_walking, '#FF0000');
                }
            });

        }

        if (typeof point.way_points_driving !== 'undefined') {

            $btnDrive = $mapArea.siblings('.btn-map_intro').find('.btn-map_car');

            if ($btnDrive.hasClass('active')) {
                drawRoute(map, point.way_points_driving, '#3832DB');
            }

            $btnDrive.on('click', function () {

                var $this = $(this);

                if (!$this.hasClass('active')) {

                    $this.parent().find('.ctrl-btn').removeClass('active');
                    $this.addClass('active');

                    drawRoute(map, point.way_points_driving, '#3832DB', point.contentPlacemarkA, point.contentPlacemarkB);
                }

            });
        }

    }


    ymaps.ready(function () {

        initMapWork($('#contacts-map').find('.contacts-tabs_content.active').find('.map-area'));

        $('.map-initer').one('click', function () {

            var a = $(this).find('a');
            setTimeout(function () {
                initMapWork($(a.attr('href')).find('.map-area'));
            }, 300);
        });

    });


});