<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult['ITEMS'])) {

    return;
}

$this->addExternalJs('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
?>

<div class="content-inner_contacts" id="contacts-map">
    <p class="contacts-title"><?= GetMessage('HOW_TO_GET_THERE') ?></p>
    <div class="workers-list_intro show">
    <ul class="contacts-nav">
        <?
        $active = true;
        foreach ($arResult['ITEMS'] as $arItem):

            $address = $arItem['PROPERTIES']['ADDRESS']['~VALUE'];
        
            $point['contentPlacemarkA'] = $arItem['PROPERTIES']['MARKER_TEXT_A']['~VALUE']['TEXT'];
            $point['contentPlacemarkB'] = $arItem['PROPERTIES']['MARKER_TEXT_B']['~VALUE']['TEXT'];

            $buttons = '';
            
            if ($arItem['PROPERTIES']['WAY_POINTS_WALKING']['VALUE']) {

                $point['way_points_walking'] = array_map(
                        function ($arPointStr) {
                    return explode(',', $arPointStr);
                }, $arItem['PROPERTIES']['WAY_POINTS_WALKING']['VALUE']
                );
                
                $buttons .= '<span class="ctrl-btn btn-map_walk active"><span><svg height="25" viewBox="0 0 48 48" width="20" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h48v48h-48z" fill="none"/><path d="M28 7.6c1.98 0 3.6-1.61 3.6-3.6s-1.62-3.6-3.6-3.6c-1.99 0-3.6 1.61-3.6 3.6s1.61 3.6 3.6 3.6zm.24 12.4h9.76v-3.6h-7.25l-4-6.66c-.59-1-1.68-1.66-2.92-1.66-.34 0-.67.05-.98.14l-10.85 3.38v10.4h3.6v-7.33l4.21-1.31-7.81 30.64h3.6l5.74-16.22 4.66 6.22v10h3.6v-12.81l-4.98-9.08 1.47-5.74 2.15 3.63z"/></svg></span></span>';
            }

            if ($arItem['PROPERTIES']['WAY_POINTS_DRIVING']['VALUE']) {

                $point['way_points_driving'] = array_map(
                        
                        function ($arPointStr) { return explode(',', $arPointStr); }, 
                        $arItem['PROPERTIES']['WAY_POINTS_DRIVING']['VALUE']
                );
                
                $buttons .= '<span class="ctrl-btn btn-map_car"><span><svg height="22" viewBox="0 0 48 48" width="21" xmlns="http://www.w3.org/2000/svg"><path d="M37.84 12.02c-.41-1.18-1.53-2.02-2.84-2.02h-22c-1.31 0-2.43.84-2.84 2.02l-4.16 11.98v16c0 1.1.9 2 2 2h2c1.11 0 2-.9 2-2v-2h24v2c0 1.1.9 2 2 2h2c1.11 0 2-.9 2-2v-16l-4.16-11.98zm-24.84 19.98c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm22 0c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm-25-10l3-9h22l3 9h-28z"/><path d="M0 0h48v48h-48z" fill="none"/></svg></span></span>';
            }
            
            if (!$point['way_points_driving'] && !$point['way_points_walking']) {
                
                $point['center'] = explode(',', $arItem['PROPERTIES']['MAP']['VALUE']);
            }

            $content .= '<div class="contacts-tabs_content ' . ($active ? 'active' : '') . '" id="contacts-' . $arItem['ID'] . '">';


            if (strlen($address) > 0) {

                $content .= $address;
            }

            $content .= '<div class="map">';
            if ($buttons) {
                
                $content .= '<div class="btn-map_intro">';
                $content .= $buttons;
                $content .= '</div>';
                
            }
            $content .= '<div id="map-'.$arItem['ID'].'" data-point=\'' . Bitrix\Main\Web\Json::encode($point) . '\' class="map-area"></div>';
            $content .= '</div>';

            $content .= '</div>';
            ?>
            <li <? if ($active): ?>class="active"<? else: ?>class="map-initer"<? endif ?>><a href="#contacts-<?= $arItem['ID'] ?>"><?= $arItem['NAME'] ?></a></li>
            <? $active = false;
        endforeach ?>
    </ul>
            <?= $content ?>
    </div>
</div>