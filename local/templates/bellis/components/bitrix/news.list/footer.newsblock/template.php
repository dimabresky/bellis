<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (empty($arResult['ITEMS'])) {
    return;
} ?>

<div class="<? if ($arParams['ORIENTATION_RIGHT'] == 'Y'): ?>news-block_right<? else: ?>news-block_list news-block_text<? endif ?>">
    <p class="blocktitle"><a href="/news/"><?= GetMessage('MAIN_VERTICAL_TITLE') ?></a></p>
    <ul class="news-block_list">
        <?
        foreach ($arResult['ITEMS'] as $arItem):

            $arImg = null;
            if ($arItem['PREVIEW_PICTURE']['ID'] > 0) {
                $arImg = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 160, 'height' => 87), BX_RESIZE_IMAGE_EXACT);
            }
            
            ?>
            <li>
                <div class="news-thumb"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arImg['src'] ?>" alt="<?= $arItem['NAME'] ?>" /></a></div>
                <div class="news-content">
                    <p class="news-content_date"><?= \bellis\formateDateByLang($arItem['ACTIVE_FROM']) ?></p>
                    <p class="news-content_title"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></p>
                </div>
            </li>
<? endforeach ?>


    </ul>
</div>