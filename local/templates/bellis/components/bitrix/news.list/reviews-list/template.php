<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
    <ul class="reviews-list">
        <?
        foreach ($arResult["ITEMS"] as $arItem):

            $arPrevPic = null;
            if ($arItem['PREVIEW_PICTURE']['ID'] > 0) {
                $arPrevPic = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 124, 'height' => 80), BX_RESIZE_IMAGE_EXACT);
            }

            $arDetPic = $arPrevPic;
            if ($arItem['PREVIEW_PICTURE']['ID'] > 0) {
                $arDetPic = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width' => 124, 'height' => 80), BX_RESIZE_IMAGE_EXACT);
            }

            $arPopupContent[] = '<div class="popup-intro-child" id="popup-intro-child-' . $arItem['ID'] . '">' .
                    '<div class="who-hides popup-close"></div>' .
                    '<div class="popup-header prevent">' .
                    '<span class="prevent"><img src="' . (strlen($arDetPic['src']) > 0 ? $arDetPic['src'] : $arPrevPic['src']) . '" alt="' . $arItem['NAME'] . '" /></span>' .
                    '</div>' .
                    '<p class="popup-title">' . $arItem['NAME'] . '</p>' .
                    '<p class="popup-introtext">' . $arItem['PROPERTIES']['POSITION']['VALUE'] . '</p>' .
                    '<div class="popup-text">' .
                    $arItem['DETAIL_TEXT'] .
                    '</div>' .
                    '</div>';

            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <li>
                <div class="reviews-list_logo"><span><img src="<?= $arPrevPic['src'] ?>" alt="<?= $NAME ?>" /></span></div>
                <div class="reviews-list_info">
                    <p class="reviews-list_name"><?= $arItem['NAME'] ?></p>
                    <p class="reviews-list_position"><?= $arItem['PROPERTIES']['POSITION']['VALUE'] ?></p>
                    <div class="preview-text-review">
                        <?= $arItem['PREVIEW_TEXT'] ?>
                    </div>
                    <p data-href="#popup-intro-child-<?= $arItem['ID'] ?>" class="reviews-list_link"><?= GetMessage('SHOW_ALL') ?></p>
                </div>
            </li>  
        <? endforeach; ?>
    </ul>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>

<div class="popup-bg"></div>
<div class="popup-content">
    <div class="popup-intro">
        <?= implode('', $arPopupContent) ?>    
    </div>
</div>
