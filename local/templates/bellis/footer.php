<? if ($APPLICATION->GetDirProperty('IS_STATIC_PAGE') == 'Y'): ?>
    </div> <!-- end .text-page -->
<? endif; ?>        
</div>
</div>
</div>
<?

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<?
$APPLICATION->IncludeComponent(
        "bitrix:main.include", ".default", array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "news_block",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_TEMPLATE" => "",
    "COMPONENT_TEMPLATE" => ".default"
        ), false
);
?>
<footer>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:menu", "main.bottom", array(
        "ROOT_MENU_TYPE" => "top",
        "MAX_LEVEL" => "2",
        "CHILD_MENU_TYPE" => "inner",
        "USE_EXT" => "N",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "COMPONENT_TEMPLATE" => "main.bottom"
            ), false
    );
    ?>
    <div class="container">
        <ul class="footer-info_list">
            <li>
<? $APPLICATION->IncludeFile(SITE_DIR . "/includes/requisites.php", Array(), array('MODE' => 'html')); ?>	
            </li>
            <li>
<? $APPLICATION->IncludeFile(SITE_DIR . "/includes/phone.php", Array(), array('MODE' => 'html')); ?>
            </li>
            <li>
<? $APPLICATION->IncludeFile(SITE_DIR . "/includes/email.php", Array(), array('MODE' => 'html')); ?>
            </li>
            <li>
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:search.title", "sfooter", Array(
                    "CATEGORY_0" => array("main", "iblock_bellis"),
                    "CATEGORY_0_TITLE" => "",
                    "CATEGORY_0_iblock_bellis" => array("all"),
                    "CATEGORY_0_main" => array(""),
                    "CHECK_DATES" => "N",
                    "CONTAINER_ID" => "title-search",
                    "INPUT_ID" => "title-search-input",
                    "NUM_CATEGORIES" => "1",
                    "ORDER" => "date",
                    "PAGE" => "#SITE_DIR#search/index.php",
                    "SHOW_INPUT" => "Y",
                    "SHOW_OTHERS" => "N",
                    "TOP_COUNT" => "5",
                    "USE_LANGUAGE_GUESS" => "Y"
                        )
                );
                ?>
            </li>
        </ul>
    </div>
    <div class="copyright">
        <div class="container">
            <p class="developer"><? $APPLICATION->IncludeFile(SITE_DIR . "/includes/copyright.php", Array(), array('MODE' => 'html')); ?></p>
        </div>
    </div>	
</footer>
</div>
</div>
<? $APPLICATION->ShowPanel() ?>	
</body>
</html>
